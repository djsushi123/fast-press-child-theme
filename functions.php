 <?php
 
 add_action( 'wp_enqueue_scripts', 'fast_press_child_enqueue_styles' );
 function fast_press_child_enqueue_styles() {
     $parenthandle = 'fast-style-css'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
     $theme = wp_get_theme();
     wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css', 
         array(),  // if the parent theme code has a dependency, copy it to here
         $theme->parent()->get('Version')
     );
     wp_enqueue_style('child-martin-css', get_stylesheet_directory_uri() . '/martin.css',
         array( $parenthandle ),
         $theme->get('Version') // this only works if you have Version in the style header
     );
     wp_enqueue_style('child-masoud-css', get_stylesheet_directory_uri() . '/masoud.css',
         array( $parenthandle ),
         $theme->get('Version') // this only works if you have Version in the style header
     );
 }